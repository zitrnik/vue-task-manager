Vue.component('test', {
props: ['text'],
template: '<div style="border: 1px solid black; font-family: sans-serif; padding: 5px;">{{text}}</div>'
});

var app = new Vue({
    el: '#app',
    components: {
       
    },
    data: {
        test: 1,
        tasks: {
            tasks1: [
                {
                    id: 1,
                    text: 'Test text 1'
                },
                {
                    id: 2,
                    text: 'Test text 2'
                }
            ],
            tasks2: [
                {
                    id: 3,
                    text: 'Tasks2 text 1'
                },
                {
                    id: 4,
                    text: 'Tasks2 text 2'
                }
            ]
        }
    },
    methods: {

    }
})