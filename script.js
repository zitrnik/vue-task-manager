Vue.component('task-unit', {
    data: function () {
        return {
            btnClass: 'fa-edit',
            taskText: ''
        }
    },
    methods: {
        removeTask(n1) {
            console.log(this.$el.getAttribute('data-col'));
            if (this.$el.getAttribute('data-col')=='backlog') {
                 app.taskList.backlog.splice(n1, 1);
            } else if (this.$el.getAttribute('data-col') == 'inprogress') {
                app.taskList.inprogress.splice(n1, 1);
            } else if (this.$el.getAttribute('data-col') == 'done') {
                app.taskList.done.splice(n1, 1);
            }
            app.saveTasks();
        },
        editTask(n1) {
            if ((this.btnClass == 'fa-edit') && (!app.isEdit)) {
                app.isEdit = !app.isEdit;
                this.$el.childNodes[1].classList.toggle("activeText");
                this.$el.childNodes[1].removeAttribute('disabled');
                this.btnClass =
                    'fa-save';
            } else if (this.btnClass == 'fa-save') {
                app.isEdit = !app.isEdit;
                this.$el.childNodes[1].classList.toggle("activeText");
                this.$el.childNodes[1].setAttribute('disabled', 'true');
                this.btnClass =
                    'fa-edit';
                
                if (this.$el.getAttribute('data-col') == 'backlog') {
                    var add = {
                        id: app.taskList.backlog[n1].id,
                        task: this.taskText
                    };
                    app.taskList.backlog.splice(n1, 1, add);
                    
                } else if (this.$el.getAttribute('data-col') == 'inprogress') {
                    var add = {
                        id: app.taskList.inprogress[n1].id,
                        task: this.taskText
                    };
                    app.taskList.inprogress.splice(n1, 1, add);
                } else if (this.$el.getAttribute('data-col') == 'done') {
                    var add = {
                        id: app.taskList.done[n1].id,
                        task: this.taskText
                    };
                    app.taskList.done.splice(n1, 1, add);
                }
                app.saveTasks();
            }
        },
        getText() {
            this.taskText = this.$el.childNodes[1].value;
        }
    },
    props: ['id', 'text', 'n'],
    template: '<div class="task-box"><h2>Task #{{id}}</h2><textarea disabled="true" @keyup="getText()" >{{text}}</textarea><div class= "task-box-btn"><i class = "fas" :class="btnClass" @click="editTask(n)" @click="getText()"></i><i class = "fas fa-times-circle" v-on:click="removeTask(n)"></i></div></div>'
})

var app = new Vue({
    el: '#app',
    data: {
        taskList: {
            backlog: [
                {
                     id: '0',
                     task: 'Пример задачи'
                }
                
            ],
            inprogress: [
               
            ],
            done: [
                
            ]
        },
        newTask: null,
        currentId: 1,
        isEdit: false
    },
    mounted() {
        if (localStorage.getItem('taskList')) {
            try {
                this.taskList = JSON.parse(localStorage.getItem('taskList'));
            } catch (e) {
                localStorage.removeItem('taskList');    
            }
        }
        if (localStorage.getItem('currentId')) {
            try {
                this.currentId = JSON.parse(localStorage.getItem('currentId'));
            } catch (e) {
                localStorage.removeItem('currentId');
            }
        }
    },
    methods: {
        addTask() {
            console.log(this.newTask);
            if (!this.newTask) {
                return;
            }
            var newTask = {
                id: this.currentId,
                task: this.newTask
            };
            this.currentId++;
            this.taskList.backlog.push(newTask);
            this.newTask = '';
            this.saveTasks();
        },
        
        saveTasks() {
            const parsed = JSON.stringify(this.taskList);
            localStorage.setItem('taskList', parsed);
            localStorage.setItem('currentId', this.currentId);
            console.log('Saved all');

        },
        info() {
            var elem = document.querySelector('header')  ;
            if ((window.matchMedia("(max-width: 1240px)").matches) && (window.matchMedia("(min-width: 660px)").matches))
            elem.style.height='180px';
        },
        info_back() {
            var elem = document.querySelector('header');
            if (window.matchMedia("(max-width: 1240px)").matches)
                elem.style.height = '180px';
            elem.style.height = '110px';
        },
        sub_backlog() {
            var elem = document.querySelector('.backlog');
            var diff = elem.offsetTop - 160;
            window.scrollTo({
                top: diff,
                behavior: "smooth"
            });
        },
        sub_inprogress() {
            var elem = document.querySelector('.inprogress');
            var diff = elem.offsetTop-160;
            window.scrollTo({
                top: diff,
                behavior: "smooth"
            });
        },
        sub_done() {
            var elem = document.querySelector('.done');
            var diff = elem.offsetTop - 160;
            window.scrollTo({
                top: diff,
                behavior: "smooth"
            });
        }
      
    }
}); 
